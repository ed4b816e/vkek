import os

import pytest
import pytest_asyncio


@pytest.fixture(scope="session", autouse=True)
def test_environ(session_mocker):
    from dotenv import load_dotenv

    load_dotenv(".env.tests")


@pytest_asyncio.fixture(autouse=True)
async def client():
    from vkek import Client

    async with Client():
        yield
