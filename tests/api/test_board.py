import pytest


@pytest.mark.asyncio
@pytest.mark.skip("Application authorization failed: method is unavailable with service token.")
async def test_board_get_comments():
    from vkek import Board

    resource = await Board.get_comments({})

    assert resource.error is None


@pytest.mark.asyncio
@pytest.mark.skip("Application authorization failed: method is unavailable with service token.")
async def test_board_get_topics():
    from vkek import Board

    resource = await Board.get_topics({})

    assert resource.error is None
