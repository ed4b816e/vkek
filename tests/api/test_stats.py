import pytest


@pytest.mark.asyncio
@pytest.mark.skip("Application authorization failed: method is unavailable with service token.")
async def test_stats_get():
    from vkek import Stats

    response = await Stats.get({"extended": 1})

    assert response.error is None
