import pytest


@pytest.mark.asyncio
async def test_users_get():
    from vkek import Users

    response = await Users.get({})

    assert response.error is None


@pytest.mark.asyncio
async def test_users_get_followers():
    from vkek import Users

    response = await Users.get_followers({"user_id": 164765810, "extended": 1})

    assert response.error is None


@pytest.mark.asyncio
async def test_users_get_subscriptions():
    from vkek import Users

    response = await Users.get_subscriptions({"user_id": 164765810, "extended": 1})

    assert response.error is None


@pytest.mark.asyncio
@pytest.mark.skip("Application authorization failed: method is unavailable with service token.")
async def test_users_report():
    from vkek import Users

    response = await Users.report({})

    assert response.error is None


@pytest.mark.asyncio
@pytest.mark.skip("Application authorization failed: method is unavailable with service token.")
async def test_users_search():
    from vkek import Users

    response = await Users.search({})

    assert response.error is None
