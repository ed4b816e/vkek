import pytest


@pytest.mark.asyncio
@pytest.mark.skip("Application authorization failed: method is unavailable with service token.")
async def test_groups_get():
    from vkek import Groups

    response = await Groups.get({})

    assert response.error is None


@pytest.mark.asyncio
@pytest.mark.skip("Application authorization failed: method is unavailable with service token.")
async def test_groups_get_banned():
    from vkek import Groups

    response = await Groups.get_banned({})

    assert response.error is None


@pytest.mark.asyncio
async def test_groups_get_members():
    from vkek import Groups

    response = await Groups.get_members({"group_id": "klikklakpage"})

    assert response.error is None


@pytest.mark.asyncio
@pytest.mark.skip("User authorization failed: anonymous or user token is required.")
async def test_groups_get_addresses():
    from vkek import Groups

    response = await Groups.get_addresses({"group_id": "klikklakpage"})

    assert response.error is None
