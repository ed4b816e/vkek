import pytest


@pytest.mark.asyncio
async def test_wall_get():
    from vkek import Wall

    response = await Wall.get({"domain": "streamfest", "count": 1})

    assert response.error is None


@pytest.mark.asyncio
@pytest.mark.skip("Not tested yet.")
async def test_wall_get_comments():
    from vkek import Wall

    response = await Wall.get_comments({"post_id": -113086748_481961})

    assert response.error is None


@pytest.mark.asyncio
@pytest.mark.skip("Not tested yet.")
async def test_wall_get_reposts():
    from vkek import Wall

    response = await Wall.get_reposts({"post_id": -113086748_481961})

    assert response.error is None
