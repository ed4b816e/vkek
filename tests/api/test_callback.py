import asyncio
import logging

import pytest


@pytest.mark.asyncio
async def test_callback_user_auth():
    from aiohttp.client_exceptions import ServerDisconnectedError
    from aiohttp import ClientSession
    from vkek import Callback
    from vkek.utils import _get_threaded_loop

    loop = asyncio.get_event_loop()

    future = loop.create_future()

    path = "/test"
    port = 4000
    test_key = "code"
    test_value = "test_code"

    async def callback():
        try:
            r = await Callback.listen(path, port)
            future.set_result(r)
        except Exception as error:
            logging.warning(error, exc_info=True)
            future.set_result("Error")

    asyncio.run_coroutine_threadsafe(callback(), _get_threaded_loop())

    async with ClientSession() as session:
        try:
            async with session.get(
                f"http://localhost:{port}{path}?{test_key}={test_value}"
            ) as response:
                assert response.status == 200
        except ServerDisconnectedError:
            pass

    while not future.done():
        await asyncio.sleep(0.1)
    assert (await future)[test_key] == test_value
