import logging

import pytest


@pytest.mark.asyncio
@pytest.mark.skip("Not yet implemented")
async def test_oauth_authorize():
    from vkek import OAuth

    code = await OAuth.authorize(
        client_id=51755865,
        redirect_uri="http://localhost:8080/",
        response_type="token",
        scope="501202911",
    )
