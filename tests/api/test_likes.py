import pytest


@pytest.mark.asyncio
async def test_likes_get_list():
    from vkek import Likes

    response = await Likes.get_list(
        {"type": "post", "item_id": 51807, "owner_id": -122887579, "extended": 1}
    )

    assert response.error is None
