import pytest


@pytest.mark.asyncio
@pytest.mark.skip("Application authorization failed: method is unavailable with service token.")
async def test_friends_get():
    from vkek import Friends

    response = await Friends.get({})

    assert response.error is None
