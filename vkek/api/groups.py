from .base import Base
from vkek.schemas.groups import GroupsGetAddressesRequest
from vkek.schemas.groups import GroupsGetAddressesResponse
from vkek.schemas.groups import GroupsGetBannedRequest
from vkek.schemas.groups import GroupsGetBannedResponse
from vkek.schemas.groups import GroupsGetMembersRequest
from vkek.schemas.groups import GroupsGetMembersResponse
from vkek.schemas.groups import GroupsGetRequest
from vkek.schemas.groups import GroupsGetResponse


class Groups(Base):
    @classmethod
    async def get(cls, params: dict | GroupsGetRequest) -> GroupsGetResponse:
        """
        Возвращает список сообществ указанного пользователя.

        :param params:
        :return:
        """
        params = GroupsGetRequest.model_validate(params, from_attributes=True)
        return GroupsGetResponse.model_validate_json(await cls._request(params, "groups.get"))

    @classmethod
    async def get_addresses(
        cls, params: dict | GroupsGetAddressesRequest
    ) -> GroupsGetAddressesResponse:
        """
        Метод возвращает адрес указанного сообщества.

        :param params:
        :return:
        """
        params = GroupsGetAddressesRequest.model_validate(params, from_attributes=True)
        return GroupsGetAddressesResponse.model_validate_json(
            await cls._request(params, "groups.getAddresses")
        )

    @classmethod
    async def get_members(cls, params: dict | GroupsGetMembersRequest) -> GroupsGetMembersResponse:
        """
        Метод возвращает список пользователей указанного сообщества.

        :param params:
        :return:
        """
        params = GroupsGetMembersRequest.model_validate(params, from_attributes=True)
        return GroupsGetMembersResponse.model_validate_json(
            await cls._request(params, "groups.getMembers")
        )

    @classmethod
    async def get_banned(cls, params: dict | GroupsGetBannedRequest) -> GroupsGetBannedResponse:
        """
        Метод возвращает список забаненных пользователей указанного сообщества.

        :param params:
        :return:
        """
        params = GroupsGetBannedRequest.model_validate(params, from_attributes=True)
        return GroupsGetBannedResponse.model_validate_json(
            await cls._request(params, "groups.getBanned")
        )
