import asyncio
from logging import getLogger

from aiohttp.web import Application
from aiohttp.web import AppRunner
from aiohttp.web import Request
from aiohttp.web import TCPSite

logger = getLogger(__file__)


class Callback:
    __slots__ = {"runner", "port"}

    def __init__(self, runner: AppRunner, port: int) -> None:
        self.runner: AppRunner = runner
        self.port: int = port

    async def __aenter__(self) -> "Callback":
        await self.runner.setup()
        site = TCPSite(self.runner, host="0.0.0.0", port=self.port)
        await site.start()
        logger.info(f"Proxy server started on {self.port}.")
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb) -> None:
        await self.runner.shutdown()
        await self.runner.cleanup()
        logger.info("Proxy server closed.")

    @classmethod
    async def listen(cls, path: str, port: int):
        result = {}

        async def proxy_endpoint(request: Request):
            result.update(request.query)

        proxy_app = Application()
        proxy_app.router.add_route("*", path, proxy_endpoint)
        runner = AppRunner(proxy_app)
        async with cls(runner, port):
            while not result:
                await asyncio.sleep(0.1)
        return result
