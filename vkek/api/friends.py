from .base import Base
from vkek.schemas.friends import FriendsGetRequest
from vkek.schemas.friends import FriendsGetResponse


class Friends(Base):
    @classmethod
    async def get(cls, params: dict | FriendsGetRequest) -> FriendsGetResponse:
        """
        Возвращает список идентификаторов друзей пользователя или расширенную информацию о друзьях пользователя (при использовании параметра fields).

        :param params:
        :return:
        """
        params = FriendsGetRequest.model_validate(params, from_attributes=True)
        return FriendsGetResponse.model_validate_json(await cls._request(params, "friends.get"))
