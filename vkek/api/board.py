from .base import Base
from vkek.schemas.board import BoardGetCommentsRequest
from vkek.schemas.board import BoardGetCommentsResponse
from vkek.schemas.board import BoardGetTopicsRequest
from vkek.schemas.board import BoardGetTopicsResponse


class Board(Base):
    @classmethod
    async def get_comments(cls, params: dict | BoardGetCommentsRequest) -> BoardGetCommentsResponse:
        """
        Возвращает список сообщений в указанной теме.

        :param params:
        :return:
        """
        params = BoardGetCommentsRequest.model_validate(params, from_attributes=True)
        return BoardGetCommentsResponse.model_validate_json(
            await cls._request(params, "board.getComments")
        )

    @classmethod
    async def get_topics(cls, params: dict | BoardGetTopicsRequest) -> BoardGetTopicsResponse:
        """
        Возвращает список тем в обсуждениях указанной группы.

        :param params:
        :return:
        """
        params = BoardGetTopicsRequest.model_validate(params, from_attributes=True)
        return BoardGetTopicsResponse.model_validate_json(
            await cls._request(params, "board.getTopics")
        )
