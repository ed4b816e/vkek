from .base import Base
from vkek.schemas.stats import StatsGetRequest
from vkek.schemas.stats import StatsGetResponse


class Stats(Base):
    @classmethod
    async def get(cls, params: dict | StatsGetRequest) -> StatsGetResponse:
        """
        Возвращает статистику сообщества или приложения.

        :param params:
        :return:
        """
        params = StatsGetRequest.model_validate(params, from_attributes=True)
        return StatsGetResponse.model_validate_json(await cls._request(params, "stats.get"))
