from .base import Base
from vkek.schemas.users import UsersGetFollowersRequest
from vkek.schemas.users import UsersGetFollowersResponse
from vkek.schemas.users import UsersGetRequest
from vkek.schemas.users import UsersGetResponse
from vkek.schemas.users import UsersGetSubscriptionsRequest
from vkek.schemas.users import UsersGetSubscriptionsResponse
from vkek.schemas.users import UsersReportRequest
from vkek.schemas.users import UsersReportResponse
from vkek.schemas.users import UsersSearchRequest
from vkek.schemas.users import UsersSearchResponse


class Users(Base):
    @classmethod
    async def get(cls, params: dict | UsersGetRequest) -> UsersGetResponse:
        """
        Возвращает расширенную информацию о пользователях.

        :param params:
        :return:
        """
        params = UsersGetRequest.model_validate(params, from_attributes=True)
        return UsersGetResponse.model_validate_json(await cls._request(params, "users.get"))

    @classmethod
    async def get_followers(
        cls, params: dict | UsersGetFollowersRequest
    ) -> UsersGetFollowersResponse:
        """
        Возвращает список идентификаторов пользователей, которые являются подписчиками пользователя.

        :param params:
        :return:
        """
        params = UsersGetFollowersRequest.model_validate(params, from_attributes=True)
        return UsersGetFollowersResponse.model_validate_json(
            await cls._request(params, "users.getFollowers")
        )

    @classmethod
    async def get_subscriptions(
        cls, params: dict | UsersGetSubscriptionsRequest
    ) -> UsersGetSubscriptionsResponse:
        """
        Возвращает список идентификаторов пользователей и публичных страниц, которые входят в список подписок пользователя.

        :param params:
        :return:
        """
        params = UsersGetSubscriptionsRequest.model_validate(params, from_attributes=True)
        return UsersGetSubscriptionsResponse.model_validate_json(
            await cls._request(params, "users.getSubscriptions")
        )

    @classmethod
    async def report(cls, params: dict | UsersReportRequest) -> UsersReportResponse:
        """
        Позволяет пожаловаться на пользователя.

        :param params:
        :return:
        """
        params = UsersReportRequest.model_validate(params, from_attributes=True)
        return UsersReportResponse.model_validate_json(await cls._request(params, "users.report"))

    @classmethod
    async def search(cls, params: dict | UsersSearchRequest) -> UsersSearchResponse:
        """
        Возвращает список пользователей в соответствии с заданным критерием поиска.

        :param params:
        :return:
        """
        params = UsersSearchRequest.model_validate(params, from_attributes=True)
        return UsersSearchResponse.model_validate_json(await cls._request(params, "users.search"))
