from logging import getLogger
from os import getenv

from aiohttp import ClientSession
from aiohttp.typedefs import LooseHeaders

logger = getLogger(__file__)


class Client:
    __slots__ = {"_headers", "_session"}

    def __init__(self, api_key: str | None = None) -> None:
        """

        :param api_key:
        """
        if api_key is None and (api_key := getenv("VK_API_KEY")) is None:
            raise
        self._headers: LooseHeaders = {"Authorization": f"Bearer {api_key}"}

    async def __aenter__(self) -> "Client":
        """

        :rtype: Client
        :return:
        """
        Client._session = ClientSession(base_url="https://api.vk.com/", headers=self._headers)
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb) -> None:
        """

        :param exc_type:
        :param exc_val:
        :param exc_tb:
        """
        await self._session.close()
