import asyncio
from logging import getLogger
from typing import Literal

from aiohttp import ClientSession
from aiohttp.typedefs import URL

from .callback import Callback
from vkek.schemas.oauth import OAuthAuthorizeRequest
from vkek.utils import _get_threaded_loop

logger = getLogger(__file__)


class OAuth:
    @classmethod
    async def authorize(
        cls,
        client_id: int,
        redirect_uri: str,
        display: Literal["page", "popup", "mobile"] | None = None,
        scope: str | None = None,
        response_type: str | None = None,
        state: str | None = None,
    ) -> str:
        params = OAuthAuthorizeRequest(
            client_id=client_id,
            display=display,
            scope=scope,
            response_type=response_type,
            state=state,
        )
        url = URL.build(
            scheme="https",
            host="oauth.vk.com",
            path="/authorize",
            query=params.model_dump(exclude_none=True),
        )

        loop = asyncio.get_event_loop()
        future = loop.create_future()

        # async def callback():
        #     try:
        #         future.set_result(await Callback.listen(params.redirect_uri.path, params.redirect_uri.port))
        #     except Exception as error:
        #         logger.warning(error, exc_info=True)
        #         future.set_result("Error")

        # asyncio.run_coroutine_threadsafe(callback(), _get_threaded_loop())
        async with ClientSession() as session:
            async with session.get(url) as response:
                data = await response.read()
                assert response.status == 200

        return data.decode("utf-8")

        # while not future.done():
        #     await asyncio.sleep(0.1)
        # return await future

    # def access_token(self, client_id: int, client_secret: str, redirect_uri: str, code: str) -> None:
    #     params = Request(client_id=client_id, client_secret=client_secret, redirect_uri=redirect_uri, code=code)
    #     url = URL.build(scheme="https", host="oauth.vk.com", path="/access_token", query=params.model_dump(exclude_none=True))
    #     logger.info(url)
