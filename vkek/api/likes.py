from .base import Base
from vkek.schemas.likes import GetLikesRequest
from vkek.schemas.likes import GetLikesResponse


class Likes(Base):
    @classmethod
    async def get_list(cls, params: dict | GetLikesRequest) -> GetLikesResponse:
        """

        :param params:
        :return:
        """
        params = GetLikesRequest.model_validate(params, from_attributes=True)
        return GetLikesResponse.model_validate_json(await cls._request(params, "likes.getList"))
