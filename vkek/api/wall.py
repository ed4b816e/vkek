from .base import Base
from vkek.schemas.wall import WallCheckCopyrightLinkRequest
from vkek.schemas.wall import WallCheckCopyrightLinkResponse
from vkek.schemas.wall import WallCloseCommentsRequest
from vkek.schemas.wall import WallCloseCommentsResponse
from vkek.schemas.wall import WallCreateCommentRequest
from vkek.schemas.wall import WallCreateCommentResponse
from vkek.schemas.wall import WallDeleteCommentRequest
from vkek.schemas.wall import WallDeleteCommentResponse
from vkek.schemas.wall import WallDeleteRequest
from vkek.schemas.wall import WallDeleteResponse
from vkek.schemas.wall import WallEditAdsStealthRequest
from vkek.schemas.wall import WallEditAdsStealthResponse
from vkek.schemas.wall import WallEditCommentRequest
from vkek.schemas.wall import WallEditCommentResponse
from vkek.schemas.wall import WallEditRequest
from vkek.schemas.wall import WallEditResponse
from vkek.schemas.wall import WallGetByIdRequest
from vkek.schemas.wall import WallGetByIdResponse
from vkek.schemas.wall import WallGetCommentRequest
from vkek.schemas.wall import WallGetCommentResponse
from vkek.schemas.wall import WallGetCommentsRequest
from vkek.schemas.wall import WallGetCommentsResponse
from vkek.schemas.wall import WallGetRepostsRequest
from vkek.schemas.wall import WallGetRepostsResponse
from vkek.schemas.wall import WallGetRequest
from vkek.schemas.wall import WallGetResponse
from vkek.schemas.wall import WallOpenCommentsRequest
from vkek.schemas.wall import WallOpenCommentsResponse
from vkek.schemas.wall import WallPinRequest
from vkek.schemas.wall import WallPinResponse
from vkek.schemas.wall import WallPostAdsStealthRequest
from vkek.schemas.wall import WallPostAdsStealthResponse
from vkek.schemas.wall import WallPostRequest
from vkek.schemas.wall import WallPostResponse
from vkek.schemas.wall import WallReportCommentRequest
from vkek.schemas.wall import WallReportCommentResponse
from vkek.schemas.wall import WallReportPostRequest
from vkek.schemas.wall import WallReportPostResponse
from vkek.schemas.wall import WallRepostRequest
from vkek.schemas.wall import WallRepostResponse
from vkek.schemas.wall import WallRestoreCommentRequest
from vkek.schemas.wall import WallRestoreCommentResponse
from vkek.schemas.wall import WallRestoreRequest
from vkek.schemas.wall import WallRestoreResponse
from vkek.schemas.wall import WallSearchRequest
from vkek.schemas.wall import WallSearchResponse
from vkek.schemas.wall import WallUnpinRequest
from vkek.schemas.wall import WallUnpinResponse


class Wall(Base):
    @classmethod
    async def check_copyright_link(
        cls, params: dict | WallCheckCopyrightLinkRequest
    ) -> WallCheckCopyrightLinkResponse:
        """
        Проверяет ссылку для указания источника.

        :param params:
        :return:
        """
        params = WallCheckCopyrightLinkRequest.model_validate(params, from_attributes=True)
        return WallCheckCopyrightLinkResponse.model_validate_json(
            await cls._request(params, "wall.checkCopyrightLink")
        )

    @classmethod
    async def close_comments(
        cls, params: dict | WallCloseCommentsRequest
    ) -> WallCloseCommentsResponse:
        """
        Выключает комментирование записи.

        :param params:
        :return:
        """
        params = WallCloseCommentsRequest.model_validate(params, from_attributes=True)
        return WallCloseCommentsResponse.model_validate_json(
            await cls._request(params, "wall.closeComments")
        )

    @classmethod
    async def create_comment(
        cls, params: dict | WallCreateCommentRequest
    ) -> WallCreateCommentResponse:
        """
        Добавляет комментарий к записи на стене.

        :param params:
        :return:
        """
        params = WallCreateCommentRequest.model_validate(params, from_attributes=True)
        return WallCreateCommentResponse.model_validate_json(
            await cls._request(params, "wall.createComment")
        )

    @classmethod
    async def delete(cls, params: dict | WallDeleteRequest) -> WallDeleteResponse:
        """
        Удаляет запись со стены.

        :param params:
        :return:
        """
        params = WallDeleteRequest.model_validate(params, from_attributes=True)
        return WallDeleteResponse.model_validate_json(await cls._request(params, "wall.delete"))

    @classmethod
    async def delete_comment(
        cls, params: dict | WallDeleteCommentRequest
    ) -> WallDeleteCommentResponse:
        """
        Удаляет комментарий к записи на стене.

        :param params:
        :return:
        """
        params = WallDeleteCommentRequest.model_validate(params, from_attributes=True)
        return WallDeleteCommentResponse.model_validate_json(
            await cls._request(params, "wall.deleteComment")
        )

    @classmethod
    async def edit(cls, params: dict | WallEditRequest) -> WallEditResponse:
        """
        Редактирует запись на стене.

        :param params:
        :return:
        """
        params = WallEditRequest.model_validate(params, from_attributes=True)
        return WallEditResponse.model_validate_json(await cls._request(params, "wall.edit"))

    @classmethod
    async def edit_ads_stealth(
        cls, params: dict | WallEditAdsStealthRequest
    ) -> WallEditAdsStealthResponse:
        """
        Позволяет отредактировать скрытую запись.

        :param params:
        :return:
        """
        params = WallEditAdsStealthRequest.model_validate(params, from_attributes=True)
        return WallEditAdsStealthResponse.model_validate_json(
            await cls._request(params, "wall.editAdsStealth")
        )

    @classmethod
    async def edit_comment(cls, params: dict | WallEditCommentRequest) -> WallEditCommentResponse:
        """
        Редактирует комментарий на стене.

        :param params:
        :return:
        """
        params = WallEditCommentRequest.model_validate(params, from_attributes=True)
        return WallEditCommentResponse.model_validate_json(
            await cls._request(params, "wall.editComment")
        )

    @classmethod
    async def get(cls, params: dict | WallGetRequest) -> WallGetResponse:
        """
        Возвращает список записей со стены пользователя или сообщества.

        :param params:
        :return:
        """
        params = WallGetRequest.model_validate(params, from_attributes=True)
        return WallGetResponse.model_validate_json(await cls._request(params, "wall.get"))

    @classmethod
    async def get_by_id(cls, params: dict | WallGetByIdRequest) -> WallGetByIdResponse:
        """
        Возвращает список записей со стен пользователей или сообществ по их идентификаторам.

        :param params:
        :return: После успешного выполнения возвращает объект, содержащий массив объектов записей на стене.
        """
        params = WallGetByIdRequest.model_validate(params, from_attributes=True)
        return WallGetByIdResponse.model_validate_json(await cls._request(params, "wall.getById"))

    @classmethod
    async def get_comment(cls, params: dict | WallGetCommentRequest) -> WallGetCommentResponse:
        """
        Получает информацию о комментарии на стене.

        :param params:
        :return: Результат возвращает объект комментария на стене.
        """
        params = WallGetCommentRequest.model_validate(params, from_attributes=True)
        return WallGetCommentResponse.model_validate_json(
            await cls._request(params, "wall.getComment")
        )

    @classmethod
    async def get_comments(cls, params: dict | WallGetCommentsRequest) -> WallGetCommentsResponse:
        """
        Возвращает список комментариев к записи на стене.

        :param params:
        :return:
        """
        params = WallGetCommentsRequest.model_validate(params, from_attributes=True)
        return WallGetCommentsResponse.model_validate_json(
            await cls._request(params, "wall.getComments")
        )

    @classmethod
    async def get_reposts(cls, params: dict | WallGetRepostsRequest) -> WallGetRepostsResponse:
        """
        Позволяет получать список репостов заданной записи.

        :param params:
        :return:
        """
        params = WallGetRepostsRequest.model_validate(params, from_attributes=True)
        return WallGetRepostsResponse.model_validate_json(
            await cls._request(params, "wall.getReposts")
        )

    @classmethod
    async def open_comments(
        cls, params: dict | WallOpenCommentsRequest
    ) -> WallOpenCommentsResponse:
        """
        Включает комментирование записи.

        :param params:
        :return:
        """
        params = WallOpenCommentsRequest.model_validate(params, from_attributes=True)
        return WallOpenCommentsResponse.model_validate_json(
            await cls._request(params, "wall.openComments")
        )

    @classmethod
    async def pin(cls, params: dict | WallPinRequest) -> WallPinResponse:
        """
        Закрепляет запись на стене (запись будет отображаться выше остальных).

        :param params:
        :return:
        """
        params = WallPinRequest.model_validate(params, from_attributes=True)
        return WallPinResponse.model_validate_json(await cls._request(params, "wall.pin"))

    @classmethod
    async def post(cls, params: dict | WallPostRequest) -> WallPostResponse:
        """
        Метод позволяет:
        • Создать запись на стене.
        • Предложить запись на стене публичной страницы.
        • Опубликовать существующую отложенную запись.

        :param params:
        :return:
        """
        params = WallPostRequest.model_validate(params, from_attributes=True)
        return WallPostResponse.model_validate_json(await cls._request(params, "wall.post"))

    @classmethod
    async def post_ads_stealth(
        cls, params: dict | WallPostAdsStealthRequest
    ) -> WallPostAdsStealthResponse:
        """
        Позволяет создать скрытую запись, которая не попадает на стену сообщества и в дальнейшем может быть использована для создания рекламного объявления типа «Запись в сообществе».

        :param params:
        :return:
        """
        params = WallPostAdsStealthRequest.model_validate(params, from_attributes=True)
        return WallPostAdsStealthResponse.model_validate_json(
            await cls._request(params, "wall.postAdsStealth")
        )

    @classmethod
    async def report_comment(
        cls, params: dict | WallReportCommentRequest
    ) -> WallReportCommentResponse:
        """
        Позволяет пожаловаться на комментарий к записи.

        :param params:
        :return:
        """
        params = WallReportCommentRequest.model_validate(params, from_attributes=True)
        return WallReportCommentResponse.model_validate_json(
            await cls._request(params, "wall.reportComment")
        )

    @classmethod
    async def report_post(cls, params: dict | WallReportPostRequest) -> WallReportPostResponse:
        """
        Позволяет пожаловаться на запись.

        :param params:
        :return:
        """
        params = WallReportPostRequest.model_validate(params, from_attributes=True)
        return WallReportPostResponse.model_validate_json(
            await cls._request(params, "wall.reportPost")
        )

    @classmethod
    async def repost(cls, params: dict | WallRepostRequest) -> WallRepostResponse:
        """
        Копирует объект на стену пользователя или сообщества.

        :param params:
        :return:
        """
        params = WallRepostRequest.model_validate(params, from_attributes=True)
        return WallRepostResponse.model_validate_json(await cls._request(params, "wall.repost"))

    @classmethod
    async def restore(cls, params: dict | WallRestoreRequest) -> WallRestoreResponse:
        """
        Восстанавливает удалённую запись на стене пользователя или сообщества.

        :param params:
        :return:
        """
        params = WallRestoreRequest.model_validate(params, from_attributes=True)
        return WallRestoreResponse.model_validate_json(await cls._request(params, "wall.restore"))

    @classmethod
    async def restore_comment(
        cls, params: dict | WallRestoreCommentRequest
    ) -> WallRestoreCommentResponse:
        """
        Восстанавливает удаленный комментарий к записи на стене.

        :param params:
        :return:
        """
        params = WallRestoreCommentRequest.model_validate(params, from_attributes=True)
        return WallRestoreCommentResponse.model_validate_json(
            await cls._request(params, "wall.restoreComment")
        )

    @classmethod
    async def search(cls, search_params: dict | WallSearchRequest) -> WallSearchResponse:
        """
        Позволяет искать записи на стене в соответствии с заданными критериями.

        :param search_params:
        :return:
        """
        search_params = WallSearchRequest.model_validate(search_params, from_attributes=True)
        return WallSearchResponse.model_validate_json(
            await cls._request(search_params, "wall.search")
        )

    @classmethod
    async def unpin(cls, params: dict | WallUnpinRequest) -> WallUnpinResponse:
        """
        Отменяет закрепление записи на стене.

        :param params:
        :return:
        """
        params = WallUnpinRequest.model_validate(params, from_attributes=True)
        return WallUnpinResponse.model_validate_json(await cls._request(params, "wall.unpin"))
