from .client import Client
from vkek.schemas.request import Request


class Base(Client):
    @classmethod
    async def _request(cls, params: Request, method: str) -> bytes:
        """

        :param params:
        :param method:
        :return:
        """
        params = params.model_dump(exclude_none=True)
        path = "/method/" + method
        async with cls._session.request(method="GET", url=path, params=params) as response:
            return await response.read()
