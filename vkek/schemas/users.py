from typing import Literal

from pydantic import BaseModel
from pydantic import Field

from .groups import Group
from .request import Request
from .response import ItemsResponse
from .response import Response
from vkek.literals.base import Bin
from vkek.literals.base import Deactivated
from vkek.literals.users import UserSex
from vkek.literals.users import UserStatus


class User(BaseModel):
    id: int
    first_name: str
    last_name: str
    deactivated: Deactivated = None
    is_closed: bool
    can_access_closed: bool
    about: str | None = None
    activities: str | None = None
    bdate: str | None = None
    blacklisted: Bin | None = None
    blacklisted_by_me: Bin | None = None
    books: list[str] | None = None
    can_post: Bin | None = None


class UsersGetSubscriptionsRequest(Request):
    user_id: int
    offset: int = Field(default=0, ge=0)
    count: int = Field(default=100, ge=0)
    extended: Bin | None = 0
    fields: list[str] | None = []


class UsersResponse(BaseModel):
    count: int
    items: list[int]


class GroupsResponse(BaseModel):
    count: int
    items: list[int]


class UsersGetSubscriptionsResponseData(BaseModel):
    users: UsersResponse | None = None
    groups: GroupsResponse | None = None
    items: list[User | Group] | None = None


class UsersGetSubscriptionsResponse(Response):
    response: UsersGetSubscriptionsResponseData


class UsersGetRequest(Request):  # TODO
    pass


class UsersGetResponse(Response):  # TODO
    pass


class UsersGetFollowersRequest(Request):
    user_id: int
    offset: int = Field(default=0, ge=0)
    count: int = Field(default=100, ge=0)
    name_case: str | None = None  # TODO
    fields: list[str] | None = []


class UsersGetFollowersResponse(Response):
    items: list[User] | None = None


class UsersReportRequest(Request):  # TODO
    pass


class UsersReportResponse(Response):  # TODO
    pass


class UsersSearchRequest(Request):  # TODO
    q: str | None = None
    sort: Bin | None = None  # TODO
    offset: int = Field(default=0, ge=0)
    count: int = Field(default=100, ge=0, le=1000)
    fields: list[str] | None = None
    city: int | None = Field(default=None, ge=0)
    city_id: int | None = Field(default=None, ge=0)
    country: int | None = Field(default=None, ge=0)
    country_id: int | None = Field(default=None, ge=0)
    hometown: str | None = None
    university_country: int | None = Field(default=None, ge=0)
    university: int | None = Field(default=None, ge=0)
    university_year: int | None = Field(default=None, ge=0)
    university_faculty: int | None = Field(default=None, ge=0)
    university_chair: int | None = Field(default=None, ge=0)
    sex: UserSex | None = 0
    status: UserStatus | None = None
    age_from: int | None = Field(default=None, ge=0)
    age_to: int | None = Field(default=None, ge=0)
    birth_day: int | None = Field(default=None, ge=0)
    birth_month: int | None = Field(default=None, ge=0)
    birth_year: int | None = Field(default=None, ge=0)
    online: Bin | None = None
    has_photo: Bin | None = None
    school_country: int | None = Field(default=None, ge=0)
    school_city: int | None = Field(default=None, ge=0)
    school_class: int | None = Field(default=None, ge=0)
    school: int | None = Field(default=None, ge=0)
    school_year: int | None = Field(default=None, ge=0)
    religion: str | None = None
    company: str | None = None
    position: str | None = None
    group_id: int | None = None
    from_list: list[Literal["friends", "subscriptions"]] | None = None


class UsersSearchResponse(Response):
    items: list[User] | None = None
