from pydantic import BaseModel
from pydantic import ConfigDict

from vkek.literals.base import Bin
from vkek.literals.base import Lang


class Request(BaseModel):
    lang: Lang | None = "ru"
    test_mode: Bin | None = 0
    v: str = "5.131"

    model_config = ConfigDict()
