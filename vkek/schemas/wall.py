import datetime
from typing import Literal

from pydantic import BaseModel
from pydantic import Field

from .groups import Group
from .items import OwnedItem
from .photos import PhotoItem
from .request import Request
from .response import ItemsResponse
from .response import Response
from .users import User
from vkek.literals.base import Bin
from vkek.literals.wall import Filter


class WallGetRequest(Request):
    owner_id: int | None = None
    domain: str | None = None
    offset: int = Field(default=0, ge=0)
    count: int = Field(default=100, ge=0, le=100)
    filter: Filter | None = "all"
    extended: Bin | None = 0
    fields: list[str] | None = []


class PostLikes(BaseModel):
    can_like: Bin
    count: int
    user_likes: int
    can_publish: Bin
    repost_disabled: bool


class PostViews(BaseModel):
    count: int


class PostReposts(BaseModel):
    count: int
    user_reposted: int


class PostComments(BaseModel):
    can_post: int
    count: int
    groups_can_post: bool


class PostAttachment(BaseModel):
    type: str
    photo: PhotoItem


class PostSource(BaseModel):
    type: str


class PostItem(OwnedItem):
    owner_id: int
    likes: PostLikes
    views: PostViews
    reposts: PostReposts
    comments: PostComments
    attachments: list[PostAttachment]
    marked_as_ads: bool
    short_text_rate: float
    hash: str
    date: int
    edited: int
    from_id: int
    post_source: PostSource
    post_type: str
    text: str


class WallGetResponseData(ItemsResponse):
    items: list[PostItem]


class WallGetResponse(Response):
    response: WallGetResponseData


class PostAttachmentInfo(BaseModel):
    type: str
    owner_id: int = Field(lt=0)
    media_id: int


class WallPostRequest(Request):
    owner_id: int | None = None
    friends_only: Bin | None = None
    from_group: Bin | None = None
    message: str | None = None
    attachments: list[PostAttachmentInfo] | None = None
    primary_attachments: PostAttachmentInfo | None = None
    services: list[str] | None = None
    signed: Bin | None = None
    publish_date: datetime.datetime | None = None
    lat: int | None = Field(default=None, ge=-90, le=90)
    long: int | None = Field(default=None, ge=-180, le=180)
    place_id: int | None = Field(default=None, ge=0)
    post_id: int | None = Field(default=None, ge=0)
    guid: str | None = None
    mark_as_ads: Bin | None = None
    close_comments: Bin | None = None
    donut_paid_duration: int | None = None
    mute_notifications: Bin | None = None
    copyright: str | None = None
    topic_id: int | None = Field(default=None, ge=0)


class WallPostResponseData(BaseModel):
    post_id: int


class WallPostResponse(Response):
    response: WallPostResponseData


class WallCheckCopyrightLinkRequest(Request):  # TODO
    pass


class WallCheckCopyrightLinkResponse(Response):  # TODO
    pass


class WallCloseCommentsRequest(Request):  # TODO
    pass


class WallCloseCommentsResponse(Response):  # TODO
    pass


class WallCreateCommentRequest(Request):  # TODO
    pass


class WallCreateCommentResponse(Response):  # TODO
    pass


class WallDeleteCommentRequest(Request):  # TODO
    pass


class WallDeleteCommentResponse(Response):  # TODO
    pass


class WallDeleteRequest(Request):  # TODO
    pass


class WallDeleteResponse(Response):  # TODO
    pass


class WallEditAdsStealthRequest(Request):  # TODO
    pass


class WallEditAdsStealthResponse(Response):  # TODO
    pass


class WallEditCommentRequest(Request):  # TODO
    pass


class WallEditCommentResponse(Response):  # TODO
    pass


class WallEditRequest(Request):  # TODO
    pass


class WallEditResponse(Response):  # TODO
    pass


class WallGetByIdRequest(Request):  # TODO
    pass


class WallGetByIdResponse(Response):  # TODO
    pass


class WallGetCommentsRequest(Request):
    owner_id: int
    post_id: int | None = Field(default=None, ge=0)
    need_likes: Bin | None = None
    start_comment_id: int | None = Field(default=None, ge=0)
    offset: int | None = Field(default=0, ge=0)
    count: int | None = Field(default=10, ge=0, le=100)
    sort: Literal["asc", "desc"] | None = None
    preview_length: int | None = Field(default=None, ge=0)
    extended: Bin | None = None
    fields: list[str] | None = None
    comment_id: int | None = Field(default=None, ge=0)
    thread_items_count: int | None = None


class WallGetCommentsResponseData(BaseModel):
    count: int
    items: list[dict]


class WallGetCommentsResponse(Response):  # TODO
    pass


class WallGetRepostsRequest(Request):
    owner_id: int | None = None
    post_id: int
    offset: int | None = Field(default=None, ge=0)
    count: int | None = Field(default=None, ge=0)


class WallGetRepostsResponseData(BaseModel):
    items: list
    profiles: list[User]
    groups: list[Group]


class WallGetRepostsResponse(Response):  # TODO
    pass


class WallPinRequest(Request):  # TODO
    pass


class WallPinResponse(Response):  # TODO
    pass


class WallGetCommentRequest(Request):  # TODO
    pass


class WallGetCommentResponse(Response):  # TODO
    pass


class WallOpenCommentsRequest(Request):  # TODO
    pass


class WallOpenCommentsResponse(Response):  # TODO
    pass


class WallPostAdsStealthRequest(Request):  # TODO
    pass


class WallPostAdsStealthResponse(Response):  # TODO
    pass


class WallUnpinRequest(Request):  # TODO
    pass


class WallUnpinResponse(Response):  # TODO
    pass


class WallRestoreRequest(Request):  # TODO
    pass


class WallRestoreResponse(Response):  # TODO
    pass


class WallRestoreCommentRequest(Request):  # TODO
    pass


class WallRestoreCommentResponse(Response):  # TODO
    pass


class WallReportCommentRequest(Request):  # TODO
    pass


class WallReportCommentResponse(Response):  # TODO
    pass


class WallReportPostRequest(Request):  # TODO
    pass


class WallReportPostResponse(Response):  # TODO
    pass


class WallRepostRequest(Request):  # TODO
    pass


class WallRepostResponse(Response):  # TODO
    pass


class WallSearchRequest(Request):  # TODO
    pass


class WallSearchResponse(Response):  # TODO
    pass
