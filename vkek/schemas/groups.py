"""
https://dev.vk.com/ru/reference/objects/group
"""
import datetime
from typing import Any
from typing import Literal

from pydantic import BaseModel
from pydantic import EmailStr
from pydantic import Field
from pydantic import HttpUrl
from pydantic_extra_types.phone_numbers import PhoneNumber

from .address import Address
from .request import Request
from .response import Response
from vkek.literals.base import Bin
from vkek.literals.base import Deactivated
from vkek.literals.groups import IsClosed


class GroupAddresses(BaseModel):
    is_enabled: bool
    main_address_id: int


class GroupBanInfo(BaseModel):
    end_date: int
    comment: str


class GroupCity(BaseModel):
    id: int
    title: str


class GroupContacts(BaseModel):
    user_id: int
    desc: str
    phone: PhoneNumber
    email: EmailStr


class GroupCountry(BaseModel):
    id: int
    title: str


class GroupLink(BaseModel):
    id: int
    url: HttpUrl
    name: str
    desc: str
    photo_50: HttpUrl
    photo_100: HttpUrl


class GroupPlace(BaseModel):
    id: int
    title: str
    latitude: float
    longitude: float
    type: str
    country: int
    city: int
    address: str


class Group(BaseModel):
    id: int
    name: str
    screen_name: str
    is_closed: IsClosed
    deactivated: Deactivated = None
    is_admin: Bin | None = None
    admin_level: int | None = None  # ToDO Literal
    is_member: Bin | None = None
    is_advertiser: Bin | None = None
    invited_by: int | None = None
    type: str  # ToDO Literal
    photo_50: HttpUrl
    photo_100: HttpUrl
    photo_200: HttpUrl
    activity: str | None = None
    addresses: GroupAddresses | None = None
    age_limits: int | None = None  # ToDO Literal
    ban_info: GroupBanInfo | None = None
    can_create_topic: Bin | None = None
    can_message: Bin | None = None
    can_post: Bin | None = None
    can_suggest: Bin | None = None
    can_see_all_posts: Bin | None = None
    can_upload_doc: Bin | None = None
    can_upload_story: Bin | None = None
    can_upload_video: Bin | None = None
    city: GroupCity | None = None
    contacts: list[GroupContacts] | None = None
    counters: Any | None = None  # ToDO
    country: GroupCountry | None = None
    cover: Any | None = None  # ToDO
    crop_photo: Any | None = None  # ToDO
    description: str | None = None
    fixed_post: int | None = None
    has_photo: Bin | None = None
    is_favorite: Bin | None = None
    is_hidden_from_feed: Bin | None = None
    is_messages_blocked: Bin | None = None
    links: list[GroupLink] | None = None
    main_album_id: int | None = None
    main_section: int | None = None  # ToDO Literal
    market: Any | None = None  # ToDO
    member_status: int | None = None  # ToDO Literal
    members_count: int | None = None
    place: GroupPlace | None = None
    public_date_label: str | None = None
    site: HttpUrl | None = None
    start_date: datetime.date | None = None
    finish_date: datetime.date | None = None
    status: str | None = None
    trending: Bin | None = None
    verified: Bin | None = None
    wall: int | None = None  # ToDO Literal
    wiki_page: str | None = None


class GroupsGetRequest(Request):
    pass


class GroupsGetResponse(Response):
    pass


class GroupsGetMembersRequest(Request):
    group_id: str
    sort: Literal["id_asc", "id_desc", "time_asc", "time_desc"] = None
    offset: int | None = Field(0, ge=0)
    count: int | None = Field(100, ge=0)
    fields: list[str] | None = None
    filter: Literal["friends", "unsure", "managers", "donut"] | None = None


class GroupsGetMembersResponseData(BaseModel):
    count: int
    items: list[int]
    role: Literal["advertiser", "moderator", "editor", "administrator", "creator"] | None = None


class GroupsGetMembersResponse(Response):
    response: GroupsGetMembersResponseData | None = None


class GroupsGetAddressesRequest(Request):
    group_id: str
    address_ids: list[str] | None = None
    latitude: float | None = Field(None, ge=-90, le=90)
    longitude: float | None = Field(None, ge=-180, le=180)
    offset: int | None = Field(0, ge=0)
    count: int | None = Field(100, ge=0)
    fields: list[str] | None = None


class GroupsGetAddressesResponseData(BaseModel):
    count: int
    items: list[Address]


class GroupsGetAddressesResponse(Response):
    pass


class GroupsGetBannedRequest(Request):
    pass


class GroupsGetBannedResponse(Response):
    pass
