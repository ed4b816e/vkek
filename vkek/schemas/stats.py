import datetime
from typing import Literal

from pydantic import BaseModel
from pydantic import Field

from .request import Request
from .response import Response
from vkek.literals.base import Bin


class StatsActivity(BaseModel):
    comments: int = Field(ge=0)
    copies: int = Field(ge=0)
    hidden: int = Field(ge=0)
    likes: int = Field(ge=0)
    subscribed: int = Field(ge=0)
    unsubscribed: int = Field(ge=0)


class StatsVisitors(BaseModel):
    views: int = Field(ge=0)
    visitors: int = Field(ge=0)


class StatsSex(BaseModel):
    value: Literal["m", "f"]
    count: int


class StatsAge(BaseModel):
    value: Literal["12-18", "18-21", "21-24", "24-27", "27-30", "30-35", "35-45", "45-100"]
    count: int


class StatsSexAge(BaseModel):
    value: str
    count: int


class StatsCiti(BaseModel):
    name: str
    citi_id: int
    count: int


class StatsCountry(BaseModel):
    name: str
    country_id: int
    code: str
    count: int


class StatsReach(BaseModel):
    reach: int = Field(ge=0)
    reach_subscribers: int = Field(ge=0)
    mobile_reach: int = Field(ge=0)
    sex: list[StatsSex]
    age: list[StatsAge]
    sex_age: list[StatsSexAge]
    cities: list[StatsCiti]
    countries: list[StatsCountry]


class Stats(BaseModel):
    activity: StatsActivity
    period_from: datetime.datetime
    period_to: datetime.datetime
    visitors: StatsVisitors
    reach: StatsReach


class StatsGetRequest(Request):
    group_id: int | None = Field(None, ge=0)
    app_id: int | None = Field(None, ge=0)
    date_from: datetime.date | None = None
    date_to: datetime.date | None = None
    timestamp_from: datetime.datetime | None = None
    timestamp_to: datetime.datetime | None = None
    interval: Literal["day", "week", "month", "year", "all"] | None = "day"
    intervals_count: int | None = Field(None, ge=0)
    filters: str | None = None
    stats_groups: Literal["activity", "visitors", "reach"] | None = None
    extended: Bin | None = None


class StatsGetResponse(Response):
    response: Stats | None = None
