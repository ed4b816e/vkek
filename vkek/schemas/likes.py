from pydantic import BaseModel
from pydantic import Field

from .items import Item
from .request import Request
from .response import ItemsResponse
from .response import Response
from .serializers import HttpUrl
from vkek.literals.base import Bin
from vkek.literals.base import ItemType
from vkek.literals.likes import Filter
from vkek.literals.likes import SkipOwn


class GetLikesRequest(Request):
    type: ItemType
    owner_id: int | None = Field(
        None,
        description="Идентификатор владельца объекта:\n"
        "• Идентификатор пользователя, если объект принадлежит пользователю.\n"
        "• Идентификатор сообщества (со знаком «минус»), если объект принадлежит сообществу.\n"
        "• Идентификатор приложения, если параметр type имеет значение sitepage.\n"
        "Значение по умолчанию: если параметр не задан, он имеет значение идентификатора текущего "
        "пользователя или идентификатора текущего приложения, если параметр type имеет значение "
        "sitepage.",
    )
    item_id: int | None = None  # ToDO
    page_url: HttpUrl | None = None  # ToDO
    filter: Filter | None = None  # ToDO
    friends_only: Bin = 0  # ToDO
    extended: Bin = 0  # ToDO
    offset: int | None = Field(default=0, ge=0)  # ToDO
    count: int | None = Field(default=1000, ge=0, le=1000)  # ToDO
    skip_own: SkipOwn | None = "false"  # ToDO


class ExtendedItem(Item):
    first_name: str
    last_name: str
    can_access_closed: bool
    is_closed: bool


class GetLikesResponseData(ItemsResponse):
    items: list[int | ExtendedItem]


class GetLikesResponse(Response):
    response: GetLikesResponseData
