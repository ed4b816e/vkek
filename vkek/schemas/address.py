from typing import Literal

from pydantic import BaseModel
from pydantic import Field
from pydantic_extra_types.phone_numbers import PhoneNumber


class Address(BaseModel):
    id: int
    country_id: int
    city_id: int
    title: str
    address: str
    additional_address: str
    latitude: float = Field(ge=-90, le=90)
    longitude: float = Field(ge=-180, le=180)
    distance: int
    phone: PhoneNumber
    time_offset: int
    metro_station_id: int
    work_info_status: Literal[
        "no_information", "temporarily_closed", "always_opened", "forever_closed", "timetable"
    ]
    timetable: dict
