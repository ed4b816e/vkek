from typing import Any

from pydantic import BaseModel
from pydantic import ConfigDict


class RequestParam(BaseModel):
    key: str
    value: str | int | float


class ErrorDetails(BaseModel):
    error_code: int
    error_msg: str
    request_params: list[RequestParam]


class Response(BaseModel):
    response: Any | None = None
    error: ErrorDetails | None = None


class ItemsResponse(BaseModel):
    count: int
    items: list[Any]

    model_config = ConfigDict(extra="allow")
