from pydantic import BaseModel

from .request import Request
from .response import Response


class FriendsGetRequest(Request):
    pass


class FriendsGetResponse(Response):
    pass
