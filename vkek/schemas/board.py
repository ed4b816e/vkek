from pydantic import BaseModel

from .request import Request
from .response import Response


class BoardGetCommentsRequest(Request):
    pass


class BoardGetCommentsResponse(Response):
    pass


class BoardGetTopicsRequest(Request):
    pass


class BoardGetTopicsResponse(Response):
    pass
