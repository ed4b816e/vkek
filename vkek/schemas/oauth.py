from typing import Literal

from pydantic import BaseModel

from .request import Request
from .response import Response
from .serializers import HttpUrl


class OAuthAuthorizeRequest(Request):
    client_id: int
    redirect_uri: HttpUrl | None = None
    display: Literal["page", "popup", "mobile"] | None = None
    scope: str | None = None
    response_type: str | None = "code"
    state: str | None = None
