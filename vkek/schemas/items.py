from pydantic import BaseModel
from pydantic import ConfigDict

from vkek.literals.base import Bin
from vkek.literals.base import ItemType


class Item(BaseModel):
    id: int
    type: ItemType

    model_config = ConfigDict(extra="allow")


class OwnedItem(Item):
    owner_id: int
