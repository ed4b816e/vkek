from typing import Annotated

from pydantic import PlainSerializer
from pydantic.networks import HttpUrl


HttpUrl = Annotated[HttpUrl, PlainSerializer(lambda url: url.unicode_string(), return_type=str, when_used="always")]
