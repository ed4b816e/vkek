from pydantic import BaseModel
from pydantic.networks import HttpUrl

from vkek.literals.photos import PhotoType


class PhotoSize(BaseModel):
    height: int
    width: int
    type: PhotoType
    url: HttpUrl


class PhotoItem(BaseModel):
    id: int
    owner_id: int
    album_id: int
    date: int
    owner_id: int
    access_key: str
    post_id: int
    sizes: list[PhotoSize]
    text: str
    user_id: int
    web_view_token: str
    has_tags: bool
