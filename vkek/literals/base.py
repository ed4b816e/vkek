from typing import Literal


Bin = Literal[0, 1]

ItemType = Literal[
    "post",
    "post_ads",
    "comment",
    "photo",
    "video",
    "note",
    "market",
    "photo_comment",
    "video_comment",
    "topic_comment",
    "market_comment",
    "sitepage",
    "profile",
]  # Тип объекта.

Deactivated = Literal["deleted", "banned"] | None

Lang = Literal["ru", "en", "uk", "be", "es", "fi", "de", "it"]
