from typing import Literal


IsClosed = Literal[
    0,  # открытое
    1,  # закрытое
    2,  # частное
]
