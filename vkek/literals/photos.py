from typing import Literal


PhotoType = Literal["s", "m", "x", "o", "p", "q", "r", "y", "z", "w"]
