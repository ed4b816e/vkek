from typing import Literal


Filter = Literal["likes", "copies"]

SkipOwn = Literal["true", "false"]
