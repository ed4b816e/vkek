from typing import Literal


Filter = Literal["suggests", "postponed", "owner", "others", "all", "donut"]
